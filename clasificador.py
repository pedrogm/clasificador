import sys


def es_correo_electronico(string):
    letr = 0
    dt = 0
    vocales = ["a", "e", "i", "o", "u"]
    caracter = "@"
    partes = string.split(caracter)
    if caracter not in string:
        return False
    else:
        for letra in partes[0]:
            letra.lower()
            if letra in vocales:
                letr += 1
        for dot in partes[1]:
            if dot == ".":
                dt += 1
        if letr > 0 and dt > 0:
            return True
        else:
            return False


def es_entero(string):
    try:
        int(string)
    except ValueError:
        return False

    return True


def es_real(entrada):
    try:
        float(entrada)
    except ValueError:
        return False

    return True


def evaluar_entrada(string):
    if es_correo_electronico(string):
        return "Es un correo electrónico."

    elif es_entero(string):
        return "Es un entero."

    elif es_real(string):
        return "Es un número real."

    elif string == "":
        return None

    else:
        return "No es ni un correo, ni un entero, ni un número real."


def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)


if __name__ == '__main__':
    main()
